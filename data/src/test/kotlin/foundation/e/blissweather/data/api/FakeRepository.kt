/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.data.api

import foundation.e.blissweather.ApiResult
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.data.util.TestUtils.getJson
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import kotlinx.serialization.json.Json

class FakeOpenWeatherRepository : OpenWeatherRepository {
    private val json = Json { ignoreUnknownKeys = true }

    override suspend fun getWeatherByCoords(coord: Coordinate): ApiResult<WeatherData> {
        val data: WeatherData = json.decodeFromString(getJson("weather_by_coords.json"))
        return ApiSuccess(data)
    }

    override suspend fun getDaysForecastByCoords(coord: Coordinate): ApiResult<WeatherDayResponse> {
        val data: WeatherDayResponse = json.decodeFromString(getJson("weather_days_forecast.json"))
        return ApiSuccess(data)
    }

    override suspend fun getWeatherByLocationName(name: String): ApiResult<WeatherData> {
        val data: WeatherData = json.decodeFromString(getJson("weather_by_location.json"))
        return ApiSuccess(data)
    }

    override suspend fun getLocationCoordsByName(
        name: String,
        limit: Int
    ): ApiResult<List<WeatherCity>> {
        val data: List<WeatherCity> = json.decodeFromString(getJson("location_coords_by_name.json"))
        return ApiSuccess(data)
    }

    override suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int
    ): ApiResult<List<WeatherCity>> {
        val data: List<WeatherCity> = json.decodeFromString(getJson("location_name_by_coords.json"))
        return ApiSuccess(data)
    }
}
