/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.location

import android.app.AlertDialog
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.WeatherCity

class LocationDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val alertDialogBuilder = AlertDialog.Builder(requireContext())
        val cities =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                arguments?.getParcelableArrayList(LOCATIONS_KEY, WeatherCity::class.java)
            } else {
                arguments?.getParcelableArrayList(LOCATIONS_KEY)
            }
        val cityNames = cities?.map { "${it.name} (${it.country})" }?.toTypedArray()

        Log.d(TAG, "onCreateDialog: $cities")

        alertDialogBuilder.apply {
            setTitle("Select location")
            setPositiveButton("Cancel") { dialog, _ -> dialog.dismiss() }
            setSingleChoiceItems(cityNames, -1) { dialog, selection ->
                val selectedCity = cities?.get(selection)
                if (selectedCity != null) {
                    setFragmentResult(
                        SELECTION_KEY,
                        bundleOf(SELECTION_VALUE to Coordinate(selectedCity.lat, selectedCity.lon))
                    )
                }

                dialog.dismiss()
            }
        }

        return alertDialogBuilder.create()
    }

    companion object {
        fun newInstance(): LocationDialog = LocationDialog()

        const val TAG = "CustomLocationPreference"
        const val LOCATIONS_KEY = "locations"
        const val SELECTION_KEY = "selection"
        const val SELECTION_VALUE = "selection_value"
    }
}
