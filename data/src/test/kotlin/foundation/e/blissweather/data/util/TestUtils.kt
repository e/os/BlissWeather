/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.data.util

import com.google.common.truth.Truth.assertThat
import java.io.File
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
object TestUtils {
    fun getJson(path: String): String {
        val uri = requireNotNull(javaClass.classLoader).getResource(path)
        val file = File(uri.path)
        return String(file.readBytes())
    }

    inline fun <reified T> assertIs(value: Any?): T {
        contract { returns() implies (value is T) }
        assertThat(value).isInstanceOf(T::class.java)
        return value as T
    }
}
