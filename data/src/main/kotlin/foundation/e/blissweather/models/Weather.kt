/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.models

import androidx.annotation.Keep
import com.squareup.moshi.Json
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String,
)

@Keep
@Serializable
data class Main(
    val temp: Double? = null,
    @Json(name = "feels_like") @SerialName("feels_like") val feelsLike: Double? = null,
    @Json(name = "temp_min") @SerialName("temp_min") val tempMin: Double? = null,
    @Json(name = "temp_max") @SerialName("temp_max") val tempMax: Double? = null,
    val pressure: Double? = null,
    val humidity: Double? = null,
)

@Keep
@Serializable
data class Wind(
    val speed: Double? = null,
    val deg: Int? = null,
)

@Keep
@Serializable
data class Clouds(
    val all: Int? = null,
)

@Keep
@Serializable
data class Rain(
    val oneHour: Double? = null,
    val threeHours: Double? = null,
)

@Keep
@Serializable
data class Snow(
    val oneHour: Double? = null,
    val threeHours: Double? = null,
)

@Keep
@Serializable
data class Sys(
    val type: Int? = null,
    val id: Int? = null,
    val country: String? = null,
    val sunrise: Long? = null,
    val sunset: Long? = null,
)

@Keep
@Serializable
data class CombinedWeatherResponse(
    val current: WeatherData? = null,
    val forecasts: WeatherDayResponse? = null,
)
