/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.worker

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import foundation.e.blissweather.widget.WeatherAppWidgetProvider
import java.util.Date

object AlarmUtils {
    private const val TAG = "AlarmUtils"

    @SuppressLint("ScheduleExactAlarm")
    fun scheduleRepeatingUpdatesCheck(context: Context, refreshInterval: Long) {
        val updateCheckIntent: PendingIntent = getRepeatingUpdatesCheckIntent(context)
        val alarmMgr = context.getSystemService(AlarmManager::class.java)
        val nextCheck: Long = System.currentTimeMillis() + (refreshInterval * 60 * 1000)
        alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, nextCheck, updateCheckIntent)
        Log.d(TAG, "Setting automatic updates check: ${Date(nextCheck)}")
    }

    private fun getRepeatingUpdatesCheckIntent(context: Context): PendingIntent {
        val intent = Intent(context, WeatherAppWidgetProvider::class.java)
        intent.setAction(WeatherAppWidgetProvider.ACTION_WEATHER_REFRESH)
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)
    }

    fun cancelRepeatingUpdatesCheck(context: Context) {
        val alarmMgr = context.getSystemService(AlarmManager::class.java)
        alarmMgr.cancel(getRepeatingUpdatesCheckIntent(context))
    }
}
