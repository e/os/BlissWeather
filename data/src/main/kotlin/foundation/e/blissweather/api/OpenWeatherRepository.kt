/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.api

import foundation.e.blissweather.ApiResult
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse

interface OpenWeatherRepository {
    suspend fun getWeatherByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherData>

    suspend fun getDaysForecastByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherDayResponse>

    suspend fun getWeatherByLocationName(
        name: String,
    ): ApiResult<WeatherData>

    suspend fun getLocationCoordsByName(
        name: String,
        limit: Int = 1,
    ): ApiResult<List<WeatherCity>>

    suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int = 1,
    ): ApiResult<List<WeatherCity>>
}
