/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather

import android.app.Application
import android.util.Log
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import foundation.e.lib.telemetry.Telemetry.init
import javax.inject.Inject
import timber.log.Timber

@HiltAndroidApp
class WeatherApp : Application(), Configuration.Provider {
    @Inject lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setMinimumLoggingLevel(Log.VERBOSE)
            .setWorkerFactory(workerFactory)
            .build()

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        if (!BuildConfig.DEBUG) {
            try {
                init(BuildConfig.SENTRY_DSN, this, true)
            } catch (e: Exception) {
                Timber.tag(TAG).e("Failed to initialize Sentry SDK: $e")
            }
        }

        Thread.currentThread().setUncaughtExceptionHandler { thread, throwable ->
            Timber.tag(TAG).e(throwable)

            val defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
            defaultUncaughtExceptionHandler?.uncaughtException(thread, throwable)
        }
    }

    companion object {
        private const val TAG = "WeatherApp"
    }
}
