# BlissWeather

Standalone Weather application for Android using [OpenWeatherMap API](https://openweathermap.org/api).

## How to install?

1. Download the latest release from [pipeline](https://gitlab.e.foundation/e/os/BlissWeather/-/pipelines/latest?ref=develop).

2. Install the release apk on your device running [/e/OS](https://e.foundation/e-os/).

3. Go to _Settings > Apps > BlissWeather > Permissions_ and grant the **Allow access all the time** location permission. Also, make sure **Use precise location** is enabled.

4. Now go to the -1 screen and add the new BlissWeather widget to the list.

5. Enjoy! It should update the weather data and display it on the widget in a while.

## License

```
/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
```
