/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import foundation.e.blissweather.ApiError
import foundation.e.blissweather.ApiException
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.models.Coordinate
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

@HiltViewModel
class SettingsViewModel
@Inject
constructor(
    private val repository: OpenWeatherRepository,
    private val appPreferences: AppPreferences
) : ViewModel() {
    private val _state = MutableSharedFlow<SettingsState>()
    val state = _state.asSharedFlow()

    private val _status = MutableSharedFlow<String>()
    val status = _status.asSharedFlow()

    fun onCustomLocationSet() {
        viewModelScope.launch { onCustomLocationSetImpl() }
    }

    fun setCoordinate(coordinate: Coordinate) {
        appPreferences.weatherCustomLocationLat = coordinate.lat
        appPreferences.weatherCustomLocationLon = coordinate.lon
    }

    private suspend fun onCustomLocationSetImpl() {
        if (!appPreferences.weatherUseCustomLocation) return

        val city = appPreferences.weatherCustomLocationCity
        if (city.isEmpty()) {
            _status.emit("Please enter a city name")
        } else {
            _state.emit(SettingsState.Loading)
            when (val result = repository.getLocationCoordsByName(city, 5)) {
                is ApiSuccess -> {
                    if (result.data.isNotEmpty()) {
                        _state.emit(SettingsState.Success(result.data))
                    }
                }
                is ApiError -> {
                    _state.emit(SettingsState.Error)
                    _status.emit("Invalid city name")
                }
                is ApiException -> {
                    _state.emit(SettingsState.Error)
                    _status.emit(result.e.message ?: "Unknown error")
                }
            }
        }
    }
}
