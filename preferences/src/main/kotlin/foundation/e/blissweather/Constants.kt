/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather

object Constants {
    const val WEATHER_SOURCE = "weather_source"
    const val WEATHER_API_KEY = "weather_api_key"
    const val WEATHER_REFRESH_INTERVAL = "weather_refresh_interval"
    const val WEATHER_USE_CUSTOM_LOCATION = "weather_use_custom_location"
    const val WEATHER_CUSTOM_LOCATION_CITY = "weather_custom_location_city"
    const val WEATHER_CUSTOM_LOCATION_LAT = "weather_custom_location_lat"
    const val WEATHER_CUSTOM_LOCATION_LON = "weather_custom_location_lon"
    const val WEATHER_ICON_SET = "weather_icons"
    const val WEATHER_USE_METRIC = "weather_use_metric"
    const val APP_PACKAGE_NAME = "foundation.e.blissweather"
}
