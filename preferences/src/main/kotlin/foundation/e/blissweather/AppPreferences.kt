/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather

import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

class AppPreferences @Inject constructor(private val sharedPrefs: SharedPreferences) {

    val all: Map<String, *>
        get() = sharedPrefs.all

    init {
        sharedPrefs.edit { remove(Constants.WEATHER_API_KEY) }
    }

    var weatherRefreshInterval: Long
        get() = sharedPrefs.getString(Constants.WEATHER_REFRESH_INTERVAL, "15")!!.toLong()
        set(value) {
            sharedPrefs.edit {
                putString(Constants.WEATHER_REFRESH_INTERVAL, value.toString().trim())
            }
        }

    var weatherUseCustomLocation: Boolean
        get() = sharedPrefs.getBoolean(Constants.WEATHER_USE_CUSTOM_LOCATION, false)
        set(value) {
            sharedPrefs.edit { putBoolean(Constants.WEATHER_USE_CUSTOM_LOCATION, value) }
        }

    var weatherCustomLocationCity: String
        get() = sharedPrefs.getString(Constants.WEATHER_CUSTOM_LOCATION_CITY, "")!!
        set(value) {
            sharedPrefs.edit { putString(Constants.WEATHER_CUSTOM_LOCATION_CITY, value.trim()) }
        }

    var weatherCustomLocationLat: Double
        get() = sharedPrefs.getString(Constants.WEATHER_CUSTOM_LOCATION_LAT, "0")!!.toDouble()
        set(value) {
            sharedPrefs.edit {
                putString(Constants.WEATHER_CUSTOM_LOCATION_LAT, value.toString().trim())
            }
        }

    var weatherCustomLocationLon: Double
        get() = sharedPrefs.getString(Constants.WEATHER_CUSTOM_LOCATION_LON, "0")!!.toDouble()
        set(value) {
            sharedPrefs.edit {
                putString(Constants.WEATHER_CUSTOM_LOCATION_LON, value.toString().trim())
            }
        }

    var weatherUseMetric: Boolean
        get() = sharedPrefs.getBoolean(Constants.WEATHER_USE_METRIC, true)
        set(value) {
            sharedPrefs.edit { putBoolean(Constants.WEATHER_USE_METRIC, value) }
        }
}
