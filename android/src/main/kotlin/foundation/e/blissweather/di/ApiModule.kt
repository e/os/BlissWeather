/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.di

import android.content.Context
import android.location.LocationManager
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dev.zacsweers.moshix.reflect.MetadataKotlinJsonAdapterFactory
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.api.OpenWeatherApi
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.api.OpenWeatherRepositoryImpl
import javax.inject.Singleton
import kotlinx.coroutines.MainScope
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder().addInterceptor(logging).build()
    }

    @Singleton
    @Provides
    @BaseRetrofit
    fun provideBaseRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .baseUrl(OpenWeatherApi.BASE_API_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(client)
            .build()

    @Singleton
    @Provides
    @GeoRetrofit
    fun provideGeoRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .baseUrl(OpenWeatherApi.BASE_GEOCODING_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(client)
            .build()

    @Singleton
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().add(MetadataKotlinJsonAdapterFactory()).build()

    @Singleton
    @Provides
    fun provideBaseApi(@BaseRetrofit retrofit: Retrofit): OpenWeatherApi.Base =
        retrofit.create(OpenWeatherApi.Base::class.java)

    @Singleton
    @Provides
    fun provideGeoApi(@GeoRetrofit retrofit: Retrofit): OpenWeatherApi.Geo =
        retrofit.create(OpenWeatherApi.Geo::class.java)

    @Singleton
    @Provides
    fun provideLocationManager(@ApplicationContext context: Context): LocationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    @Singleton
    @Provides
    fun provideRepository(
        baseApi: OpenWeatherApi.Base,
        geoApi: OpenWeatherApi.Geo,
        prefs: AppPreferences
    ): OpenWeatherRepository = OpenWeatherRepositoryImpl(baseApi, geoApi, prefs)

    @ApplicationScope @Provides fun provideCoroutineScope() = MainScope()
}
