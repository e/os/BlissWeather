/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.api

import foundation.e.blissweather.models.Units
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {
    interface Base {
        @GET("weather")
        suspend fun getWeatherByCoords(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<WeatherData>

        @GET("weather")
        suspend fun getWeatherByLocationName(
            @Query("q") name: String,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<WeatherData>

        @GET("forecast/daily")
        suspend fun getForecastByCoords(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
            @Query("cnt") count: Int = 5,
        ): Response<WeatherDayResponse>
    }

    interface Geo {
        @GET("direct")
        suspend fun getLocationCoordsByName(
            @Query("q") name: String,
            @Query("limit") limit: Int = 1,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<List<WeatherCity>>

        @GET("reverse")
        suspend fun getLocationNameByCoords(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("limit") limit: Int = 1,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<List<WeatherCity>>
    }

    companion object {
        const val BASE_API_URL = "https://api.openweathermap.org/data/2.5/"
        const val BASE_GEOCODING_URL = "https://api.openweathermap.org/geo/1.0/"
    }
}
