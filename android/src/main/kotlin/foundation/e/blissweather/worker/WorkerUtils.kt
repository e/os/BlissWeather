/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.worker

import android.util.Log
import foundation.e.blissweather.ApiError
import foundation.e.blissweather.ApiException
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.models.CombinedWeatherResponse
import foundation.e.blissweather.models.Coordinate
import java.util.Locale

object WorkerUtils {
    private const val TAG = "WorkerUtils"

    suspend fun getCombinedWeatherData(
        weatherRepository: OpenWeatherRepository,
        coords: Coordinate,
    ): CombinedWeatherResponse {
        val current =
            when (val result = weatherRepository.getWeatherByCoords(coords)) {
                is ApiSuccess -> result.data
                is ApiError -> {
                    Log.e(TAG, "[${result.code}] Current Weather Error: ${result.message}")
                    null
                }
                is ApiException -> {
                    Log.e(TAG, "Current Weather Error:", result.e)
                    null
                }
            }

        val reverseGeocode =
            when (val result = weatherRepository.getLocationNameByCoords(coords)) {
                is ApiSuccess -> result.data
                is ApiError -> {
                    Log.e(TAG, "[${result.code}] ReverseGeocode Error: ${result.message}")
                    null
                }
                is ApiException -> {
                    Log.e(TAG, "ReverseGeocode Error:", result.e)
                    null
                }
            }

        if (current != null && reverseGeocode != null) {
            val name =
                reverseGeocode[0]
                    .localNames
                    .getOrDefault(Locale.getDefault().language, reverseGeocode[0].name)

            if (name.isNotEmpty()) {
                current.name = name
            }
        }

        val forecasts =
            when (val result = weatherRepository.getDaysForecastByCoords(coords)) {
                is ApiSuccess -> result.data
                is ApiError -> {
                    Log.e(TAG, "[${result.code}] Forecasts Error: ${result.message}")
                    null
                }
                is ApiException -> {
                    Log.e(TAG, "Forecasts Error:", result.e)
                    null
                }
            }

        return CombinedWeatherResponse(current, forecasts)
    }
}
