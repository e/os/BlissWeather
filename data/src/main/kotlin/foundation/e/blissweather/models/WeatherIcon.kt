/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.models

import androidx.annotation.DrawableRes
import foundation.e.blissweather.data.R

enum class WeatherIcon(@DrawableRes val icon: Int) {
    CLEAR_DAY(R.drawable.weather_color_32),
    CLEAR_NIGHT(R.drawable.weather_color_31),
    CLOUDS(R.drawable.weather_color_26),
    FEW_CLOUDS_DAY(R.drawable.weather_color_28),
    FEW_CLOUDS_NIGHT(R.drawable.weather_color_27),
    RAIN(R.drawable.weather_color_40),
    SHOWER_RAIN(R.drawable.weather_color_12),
    FREEZING_RAIN(R.drawable.weather_color_35),
    SNOW(R.drawable.weather_color_43),
    THUNDERSTORM(R.drawable.weather_color_3),
    MIST(R.drawable.weather_color_22),
    DRIZZLE(R.drawable.weather_color_11);

    companion object {
        fun fromCode(code: Int) =
            when (code) {
                in 200..232 -> THUNDERSTORM
                in 300..321 -> DRIZZLE
                in 500..504 -> RAIN
                511 -> FREEZING_RAIN
                in 520..531 -> SHOWER_RAIN
                in 600..622 -> SNOW
                in 701..781 -> MIST
                800 -> CLEAR_DAY
                801 -> FEW_CLOUDS_DAY
                in 802..804 -> CLOUDS
                else -> CLEAR_DAY
            }
    }
}
