@file:Suppress("UnstableApiUsage", "DSL_SCOPE_VIOLATION")

import java.nio.file.Files
import java.nio.file.Paths

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.hiddensecretsplugin)
}

android {
    namespace = "foundation.e.blissweather.preferences"
    compileSdk = 33

    defaultConfig { minSdk = 25 }

    buildFeatures { buildConfig = true }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    // Enable NDK build
    externalNativeBuild { cmake { path("src/main/cpp/CMakeLists.txt") } }

    tasks.named("preBuild") { dependsOn("weatherKeyInject") }
}

tasks.register("weatherKeyInject") {
    val weatherApiKey = System.getenv("WEATHER_API_KEY")

    doFirst {
        // List of directories to delete
        val directories = listOf("preferences/src/main/java", "preferences/src/main/cpp")

        // Iterate through the directories and delete them
        directories.forEach { dir ->
            val path = Paths.get(dir)
            if (Files.exists(path)) {
                path.toFile().deleteRecursively()
            }
        }

        exec {
            commandLine(
                "./../gradlew",
                "hideSecret",
                "-Pkey=$weatherApiKey",
                "-PkeyName=WeatherKey",
                "-Ppackage=foundation.e.blissweather"
            )
            // Suppress output
            standardOutput = org.gradle.internal.io.NullOutputStream.INSTANCE
            errorOutput = org.gradle.internal.io.NullOutputStream.INSTANCE
        }
    }
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.google.dagger.hilt)
}
