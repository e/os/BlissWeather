@file:Suppress("UnstableApiUsage", "DSL_SCOPE_VIOLATION", "KaptUsageInsteadOfKsp")

import com.android.build.gradle.internal.api.BaseVariantOutputImpl
import java.io.FileInputStream
import java.util.Properties

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.hilt)
}

val appVersionMajor = 1
val appVersionMinor = 0
val appVersionPatch = 5
val appVersionCode = 4

android {
    namespace = "foundation.e.blissweather"
    compileSdk = 33

    val versionPropsFile = file("version.properties")
    val versionProps = Properties()

    if (!versionPropsFile.exists()) {
        versionProps["VERSION_CHANGE"] = "0"
        versionProps["VERSION_MAJOR"] = appVersionMajor.toString()
        versionProps["VERSION_MINOR"] = appVersionMinor.toString()
        versionProps["VERSION_PATCH"] = appVersionPatch.toString()
        versionProps["VERSION_CODE"] = appVersionCode.toString()
        versionProps.store(versionPropsFile.writer(), null)
    }

    fun getVersionCode(): Int {
        if (versionPropsFile.canRead()) {
            versionProps.load(FileInputStream(versionPropsFile))
            var versionChange = Integer.valueOf(versionProps["VERSION_CHANGE"].toString()) + 1
            var versionMinor = Integer.valueOf(versionProps["VERSION_MINOR"].toString())
            var versionMajor = Integer.valueOf(versionProps["VERSION_MAJOR"].toString())
            var versionPatch = Integer.valueOf(versionProps["VERSION_PATCH"].toString())
            // Up version on each 100 cycles of builds
            if (versionChange >= 100) {
                versionPatch += 1
                versionChange = 0
            }
            if (versionPatch == 9) {
                versionMinor = versionPatch + 1
                versionPatch = 0
            }
            if (versionMinor == 9) {
                versionMajor += 1
                versionMinor = 0
            }
            val versionCode = Integer.valueOf(versionProps["VERSION_CODE"].toString())

            versionProps["VERSION_CHANGE"] = versionChange.toString()
            versionProps["VERSION_PATCH"] = versionPatch.toString()
            versionProps["VERSION_MINOR"] = versionMinor.toString()
            versionProps["VERSION_MAJOR"] = versionMajor.toString()
            versionProps["VERSION_CODE"] = (Integer.valueOf(versionCode) + 1).toString()
            versionProps.store(versionPropsFile.writer(), null)

            return versionCode
        }
        return appVersionCode
    }

    fun getVersionName(): String {
        if (versionPropsFile.canRead()) {
            versionProps.load(FileInputStream(versionPropsFile))

            val versionMajor = versionProps["VERSION_MAJOR"]
            val versionMinor = versionProps["VERSION_MINOR"]
            val versionPatch = versionProps["VERSION_PATCH"]

            return "${versionMajor}.${versionMinor}.${versionPatch}"
        }
        return "$appVersionMajor.$appVersionMinor.$appVersionPatch"
    }

    defaultConfig {
        applicationId = "foundation.e.blissweather"
        minSdk = 25
        targetSdk = 33
        versionCode = getVersionCode()
        versionName = getVersionName()

        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        setProperty("archivesBaseName", "${defaultConfig.applicationId}-v$versionName")
    }

    signingConfigs {
        create("platform") {
            storeFile = file("${rootDir.path}/keystore/platform.jks")
            storePassword = "platform"
            keyAlias = "platform"
            keyPassword = "platform"
        }
    }

    buildOutputs.all {
        val variantOutputImpl = this as BaseVariantOutputImpl
        val outputFileName = "BlissWeather-${variantOutputImpl.name}.apk"
        variantOutputImpl.outputFileName = outputFileName
    }

    buildTypes {
        debug {
            isShrinkResources = false
            isMinifyEnabled = false
            versionNameSuffix = "-debug"
            signingConfig = signingConfigs.getByName("platform")
        }

        release {
            isShrinkResources = true
            isMinifyEnabled = true
            versionNameSuffix = "-release"
            signingConfig = signingConfigs.getByName("platform")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

        configureEach {
            buildConfigField("String", "SENTRY_DSN", "\"${System.getenv("SENTRY_DSN")}\"")
        }
    }

    buildFeatures {
        viewBinding = true
        buildConfig = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions { jvmTarget = JavaVersion.VERSION_17.toString() }
}

dependencies {
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.lifecycle.ktx)
    implementation(libs.androidx.lifecycle.viewmodel)
    implementation(libs.androidx.preference)
    implementation(libs.androidx.work)
    implementation(libs.androidx.hilt.work)
    kapt(libs.androidx.hilt.compiler)

    implementation(libs.apache.text)

    implementation(libs.google.material)
    implementation(libs.google.dagger.hilt)
    kapt(libs.google.dagger.hilt.compiler)

    implementation(libs.square.moshi)
    implementation(libs.square.moshi.converter)
    implementation(libs.square.moshi.metadata.reflect)
    implementation(libs.square.okhttp.logging)
    implementation(libs.kotlinx.serialization)

    implementation(libs.elib)
    implementation(libs.e.telemetry)
    implementation(libs.timber)

    implementation(projects.preferences)
    implementation(projects.data)
}
