/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.util.Log
import androidx.core.location.LocationManagerCompat
import androidx.core.os.CancellationSignal
import dagger.hilt.android.qualifiers.ApplicationContext
import foundation.e.blissweather.locationPermissionEnabled
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.resume
import kotlinx.coroutines.suspendCancellableCoroutine

@SuppressLint("MissingPermission")
@Singleton
class LocationProvider
@Inject
constructor(
    @ApplicationContext private val context: Context,
    private val locMgr: LocationManager,
) {
    private fun checkPermissions(): Boolean {
        return LocationManagerCompat.isLocationEnabled(locMgr) &&
            context.locationPermissionEnabled()
    }

    private val provider =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            LocationManager.FUSED_PROVIDER
        } else {
            LocationManager.NETWORK_PROVIDER
        }

    private fun getLastLocation(): Location? {
        if (!checkPermissions()) return null

        return locMgr.getLastKnownLocation(provider)
            ?: locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER)
    }

    suspend fun getCurrentLocation(): Location? = suspendCancellableCoroutine { continuation ->
        var gpsLocation: Location? = null
        var providerLocation: Location? = null

        if (!checkPermissions()) {
            Log.i(TAG, "Location permission denied...")

            if (continuation.isActive) {
                continuation.resume(null)
            }
            return@suspendCancellableCoroutine
        }

        val cancelSignal = CancellationSignal()
        continuation.invokeOnCancellation { cancelSignal.cancel() }

        runCatching {
                LocationManagerCompat.getCurrentLocation(
                    locMgr,
                    provider,
                    cancelSignal,
                    Executors.newSingleThreadExecutor()
                ) {
                    Log.i(TAG, "$provider location update received: $it")
                    providerLocation = it
                }

                LocationManagerCompat.getCurrentLocation(
                    locMgr,
                    LocationManager.GPS_PROVIDER,
                    cancelSignal,
                    Executors.newSingleThreadExecutor()
                ) {
                    Log.i(TAG, "GPS location update received: $it")
                    gpsLocation = it
                }

                val location =
                    if (providerLocation == null && gpsLocation == null) {
                        getLastLocation()
                    } else {
                        providerLocation ?: gpsLocation
                    }

                if (continuation.isActive) {
                    continuation.resume(location)
                }
            }
            .onFailure {
                Log.i(TAG, "Error retrieving location...")
                if (continuation.isActive) {
                    continuation.resume(null)
                }
            }
    }

    companion object {
        private const val TAG = "LocationProvider"
    }
}
