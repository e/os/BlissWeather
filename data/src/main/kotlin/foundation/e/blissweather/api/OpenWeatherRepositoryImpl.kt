/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.api

import foundation.e.blissweather.ApiResult
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.Constants
import foundation.e.blissweather.Secrets
import foundation.e.blissweather.handleApi
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.Units
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import java.util.Locale
import javax.inject.Inject

class OpenWeatherRepositoryImpl
@Inject
constructor(
    private val baseApi: OpenWeatherApi.Base,
    private val geoApi: OpenWeatherApi.Geo,
    prefs: AppPreferences
) : OpenWeatherRepository {
    private val appPreferences = prefs

    private val apiKey: String = Secrets().getWeatherKey(Constants.APP_PACKAGE_NAME)

    private val unit: Units
        get() {
            return if (appPreferences.weatherUseMetric) {
                Units.METRIC
            } else {
                Units.IMPERIAL
            }
        }

    private val lang: String
        get() = Locale.getDefault().language

    override suspend fun getWeatherByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherData> = handleApi {
        baseApi.getWeatherByCoords(coord.lat, coord.lon, apiKey, unit, lang)
    }

    override suspend fun getDaysForecastByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherDayResponse> = handleApi {
        baseApi.getForecastByCoords(coord.lat, coord.lon, apiKey, unit, lang)
    }

    override suspend fun getWeatherByLocationName(
        name: String,
    ): ApiResult<WeatherData> = handleApi {
        baseApi.getWeatherByLocationName(name, apiKey, unit, lang)
    }

    override suspend fun getLocationCoordsByName(
        name: String,
        limit: Int,
    ): ApiResult<List<WeatherCity>> = handleApi {
        geoApi.getLocationCoordsByName(name, limit, apiKey, unit, lang)
    }

    override suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int,
    ): ApiResult<List<WeatherCity>> = handleApi {
        geoApi.getLocationNameByCoords(coord.lat, coord.lon, limit, apiKey, unit, lang)
    }
}
