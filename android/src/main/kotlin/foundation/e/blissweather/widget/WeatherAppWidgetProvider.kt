/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.work.BackoffPolicy
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import dagger.hilt.android.AndroidEntryPoint
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.worker.AlarmUtils
import foundation.e.blissweather.worker.WeatherUpdaterWorker
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class WeatherAppWidgetProvider : AppWidgetProvider() {
    @Inject lateinit var appPrefs: AppPreferences

    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        newOptions: Bundle?
    ) {
        refresh(context)
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
    }

    override fun onEnabled(context: Context) {
        val workMgr = WorkManager.getInstance(context.applicationContext)
        val refreshInterval = appPrefs.weatherRefreshInterval
        val constraints =
            Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()

        if (refreshInterval != 0L) {
            workMgr.enqueueUniquePeriodicWork(
                WEATHER_UPDATE,
                ExistingPeriodicWorkPolicy.UPDATE,
                // Don't use this periodic worker, it doesn't work on idle.
                PeriodicWorkRequestBuilder<WeatherUpdaterWorker>(30, TimeUnit.DAYS)
                    .setConstraints(constraints)
                    .build()
            )

            // Use AlarmManager for repeated job
            AlarmUtils.cancelRepeatingUpdatesCheck(context)
            AlarmUtils.scheduleRepeatingUpdatesCheck(context, refreshInterval)
        } else {
            workMgr
                .beginWith(
                    OneTimeWorkRequestBuilder<WeatherUpdaterWorker>()
                        .addTag(WEATHER_UPDATE)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, 2, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build()
                )
                .enqueue()

            // Cancel any repeating alarm
            AlarmUtils.cancelRepeatingUpdatesCheck(context)
        }
    }

    private fun refresh(context: Context) {
        onDisabled(context)
        onEnabled(context)
    }

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d(TAG, "onReceive: $intent")
        super.onReceive(context, intent)
        when (intent?.action) {
            ACTION_WEATHER_REFRESH,
            Intent.ACTION_LOCALE_CHANGED -> {
                refresh(context)
            }
        }
    }

    override fun onDisabled(context: Context) {
        WorkManager.getInstance(context).cancelAllWorkByTag(WEATHER_UPDATE)
    }

    companion object {
        const val TAG = "WeatherAppWidget"
        const val WEATHER_UPDATE = "weather_update_work"

        const val ACTION_WEATHER_REFRESH = "foundation.e.blissweather.widget.ACTION_WEATHER_REFRESH"
    }
}
