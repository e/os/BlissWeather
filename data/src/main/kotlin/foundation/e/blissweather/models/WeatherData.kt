/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.models

import androidx.annotation.Keep
import com.squareup.moshi.Json
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class WeatherData(
    @Json(name = "cod") @SerialName("cod") val code: Int,
    val id: Int,
    var name: String,
    val main: Main,
    val sys: Sys,
    val dt: Long,
    val coord: Coordinate,
    val visibility: Int,
    val timezone: Int,
    val weather: List<Weather>,
    val wind: Wind,
    val clouds: Clouds? = null,
    val rain: Rain? = null,
    val snow: Snow? = null,
)
