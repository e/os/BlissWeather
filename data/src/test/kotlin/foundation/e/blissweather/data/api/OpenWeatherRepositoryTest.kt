/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.data.api

import com.google.common.truth.Truth.assertThat
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.data.util.MainDispatcherRule
import foundation.e.blissweather.data.util.TestUtils.assertIs
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class OpenWeatherRepositoryTest {
    private lateinit var repository: OpenWeatherRepository
    private val coords = Coordinate(51.5072, -0.1276)
    private val locName = "London"

    @get:Rule val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        repository = FakeOpenWeatherRepository()
    }

    @Test
    fun `getWeatherByCoords should return a WeatherData`() = runTest {
        val weather = repository.getWeatherByCoords(coords)

        assertIs<ApiSuccess<WeatherData>>(weather)
        assertThat(weather.data.coord).isEqualTo(coords)
        assertThat(weather.data.weather).hasSize(1)
    }

    @Test
    fun `getDaysForecastByCoords should return a list of WeatherData`() = runTest {
        val forecast = repository.getDaysForecastByCoords(coords)

        assertIs<ApiSuccess<WeatherDayResponse>>(forecast)
        assertThat(forecast.data.list).hasSize(6)
    }

    @Test
    fun `getWeatherByLocationName should return a WeatherData`() = runTest {
        val weather = repository.getWeatherByLocationName(locName)

        assertIs<ApiSuccess<WeatherData>>(weather)
        assertThat(weather.data.name).isEqualTo(locName)
        assertThat(weather.data.weather).hasSize(1)
    }

    @Test
    fun `getLocationCoordsByName should return a list of WeatherCity`() = runTest {
        val cities = repository.getLocationCoordsByName(locName)

        assertIs<ApiSuccess<List<WeatherCity>>>(cities)
        assertThat(cities.data[0].name).isEqualTo(locName)
        assertThat(cities.data[0].lat).isWithin(0.1).of(coords.lat)
        assertThat(cities.data[0].lon).isWithin(0.1).of(coords.lon)
    }

    @Test
    fun `getLocationNameByCoords should return a list of WeatherCity`() = runTest {
        val cities = repository.getLocationNameByCoords(coords)

        assertIs<ApiSuccess<List<WeatherCity>>>(cities)
        assertThat(cities.data[0].name).isEqualTo(locName)
        assertThat(cities.data[0].lat).isWithin(0.1).of(coords.lat)
        assertThat(cities.data[0].lon).isWithin(0.1).of(coords.lon)
    }
}
