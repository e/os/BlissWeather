/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.blissweather.widget

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.RemoteViews
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.R
import foundation.e.blissweather.models.CombinedWeatherResponse
import foundation.e.blissweather.models.WeatherIcon
import foundation.e.blissweather.settings.SettingsActivity
import foundation.e.blissweather.toDirection
import foundation.e.blissweather.toKmph
import java.util.*
import kotlin.math.roundToInt
import org.apache.commons.text.WordUtils

@SuppressLint("DiscouragedApi")
fun RemoteViews.updateWeatherView(
    context: Context,
    data: CombinedWeatherResponse,
    prefs: AppPreferences,
) {
    val useMetrics = prefs.weatherUseMetric

    val tempUnit =
        context.getString(
            if (useMetrics) {
                R.string.degree_celsius
            } else {
                R.string.degree_fahrenheit
            }
        )

    val speedUnit =
        context.getString(
            if (useMetrics) {
                R.string.weather_kph
            } else {
                R.string.weather_mph
            }
        )

    this.apply {
        data.current?.apply {
            val windSpeed =
                if (useMetrics) {
                    wind.speed?.toKmph()
                } else {
                    wind.speed?.roundToInt()
                }
            setViewVisibility(R.id.weather_setup_textview, View.GONE)
            setTextViewText(R.id.location, name)
            setTextViewText(R.id.weather_summary, WordUtils.capitalize(weather[0].description))

            setTextViewText(
                R.id.humidity_and_wind_speed,
                String.format(
                    context.getString(R.string.humidity_and_wind_speed),
                    main.humidity?.roundToInt(),
                    windSpeed,
                    "$speedUnit ${wind.deg?.toDirection(context)}",
                )
            )
            setTextViewText(R.id.weather_temperature, "${main.temp?.roundToInt()}$tempUnit")
            setTextViewText(
                R.id.weather_high_and_min,
                "${main.tempMin?.roundToInt()}$tempUnit / ${main.tempMax?.roundToInt()}$tempUnit"
            )
            setImageViewResource(
                R.id.weather_current_icon,
                WeatherIcon.fromCode(weather[0].id).icon
            )
            setOnClickPendingIntent(
                R.id.widget_refresh,
                PendingIntent.getBroadcast(
                    context,
                    0,
                    Intent(context, WeatherAppWidgetProvider::class.java).apply {
                        action = WeatherAppWidgetProvider.ACTION_WEATHER_REFRESH
                    },
                    PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                )
            )
            setOnClickPendingIntent(
                R.id.widget_settings,
                PendingIntent.getActivity(
                    context,
                    0,
                    Intent(context, SettingsActivity::class.java),
                    PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                )
            )
        }

        val calendar = Calendar.getInstance()
        val currentDay = calendar.get(Calendar.DAY_OF_WEEK)
        val forecasts = data.forecasts?.list ?: emptyList()

        forecasts.forEachIndexed { index, day ->
            val nextDay = currentDay + index
            val dayName =
                calendar
                    .apply { set(Calendar.DAY_OF_WEEK, nextDay) }
                    .getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())

            val dayRes =
                context.resources.getIdentifier(
                    "weather_day${index + 1}",
                    "id",
                    context.packageName
                )

            removeAllViews(dayRes)
            addView(
                dayRes,
                RemoteViews(context.packageName, R.layout.weather_day).apply {
                    setTextViewText(R.id.forecast_day, dayName)
                    setTextViewText(
                        R.id.weather_temps,
                        "${day.temp.min.roundToInt()}$tempUnit\n${day.temp.max.roundToInt()}$tempUnit"
                    )
                    setImageViewResource(
                        R.id.weather_image,
                        WeatherIcon.fromCode(day.weather[0].id).icon
                    )
                }
            )
        }
    }
}
